import os
import subprocess
import requests
from PyQt5.QtWidgets import QApplication, QWidget, QVBoxLayout, QGridLayout, QPushButton, QLineEdit, QMessageBox, QLabel
import re
import functools
import tempfile
import zipfile
import sys

class UpdateManager:
    def __init__(self, version_label):
        self.version_label = version_label
        self.parent = version_label.parent() if version_label else None  # Ebeveyn pencereyi almak için
        self.prog_ver_check()

    def apply_update(self, temp_exe_path, current_exe_path):
        try:
            os.replace(temp_exe_path, current_exe_path)
            return True
        except Exception as e:
            print(f"Hata: {e}")
            return False

    def download_update(self, url, dest_path):
        try:
            response = requests.get(url, stream=True)
            with open(dest_path, 'wb') as file:
                for chunk in response.iter_content(chunk_size=1024):
                    if chunk:
                        file.write(chunk)
        except Exception as e:
            print(f"Hata: {e}")

    def extract_zip(self, zip_path, extract_path):
        try:
            with zipfile.ZipFile(zip_path, 'r') as zip_ref:
                zip_ref.extractall(extract_path)
            return extract_path
        except Exception as e:
            print(f"Hata: {e}")
            return None

    def get_single_exe_in_directory(self, directory):
        exe_files = [f for f in os.listdir(directory) if f.lower().endswith('.exe') and os.path.isfile(os.path.join(directory, f))]

        if len(exe_files) == 1:
            return os.path.join(directory, exe_files[0])
        else:
            return None

    def create_update_script(self, temp_exe_path, current_exe_path):
        # Bat dosyasını oluştur
        bat_content = f'@echo off\n'
        bat_content += f'taskkill /IM "{os.path.basename(current_exe_path)}" /F\n'  # Uygulama kapat
        bat_content += f'ping 127.0.0.1 -n 2 > nul\n'  # Kısa bir bekleme
        bat_content += f'start "" "{temp_exe_path}"\n'
        bat_content += f'exit\n'  # Bat dosyasından çık
        bat_path = os.path.join(tempfile.gettempdir(), "update_script.bat")
        with open(bat_path, 'w') as bat_file:
            bat_file.write(bat_content)
        return bat_path

    def run_update_script(self, bat_path):
        try:
            subprocess.run(bat_path, shell=True)
            return True
        except Exception as e:
            print(f"Hata: {e}")
            return False

    def prog_ver_check(self):
        gitlab_url = "https://gitlab.com/saydut/power-plans-with-python/-/raw/main/version.txt?ref_type=heads"

        local_version = self.version_label.text().split(":")[-1].strip()

        try:
            response = requests.get(gitlab_url)
            remote_version_info = response.text.strip().split('\n')
            remote_version = remote_version_info[0]
            release_notes_start = response.text.find('*')
            release_notes = response.text[release_notes_start:].split('link=')[0].strip()
            update_link_start = response.text.find('link=')
            update_link = response.text[update_link_start + 5:].strip()

            if remote_version > local_version:
                # Yeni sürüm mevcut! Güncelleme yapılabilir.
                notes_response = requests.get(gitlab_url)
                release_notes = self.extract_release_notes(notes_response.text)

                result = QMessageBox.question(self.parent, "Yeni Sürüm", f"Yeni bir sürüm mevcut!\nGüncelleme yapmak ister misiniz?\n\n{release_notes}", QMessageBox.Yes | QMessageBox.No)

                if result == QMessageBox.Yes:
                    # Kullanıcı evet dediği durum
                    print("Güncelleme işlemleri burada başlayacak.")
                    temp_zip_path = os.path.join(tempfile.gettempdir(), "temp_update.zip")
                    temp_exe_path = os.path.join(tempfile.gettempdir(), "temp_update.exe")

                    # Zip dosyasını indir ve çıkar
                    self.download_update(update_link, temp_zip_path)
                    extracted_path = self.extract_zip(temp_zip_path, tempfile.gettempdir())  # Zip dosyasını çıkar
                    exe_path = self.get_single_exe_in_directory(extracted_path)  # Çıkarılan dizindeki tek exe dosyasını al

                    # Bat dosyasını oluştur ve çalıştır
                    bat_path = self.create_update_script(exe_path, sys.executable)
                    self.run_update_script(bat_path)
                    
                    # Güncelleme script'i çalıştıktan sonra programı kapat
                    sys.exit()
            else:
                print("Güncelleme yok.")
        except requests.RequestException as e:
            print(f"Hata: {e}")

    def extract_release_notes(self, version_txt_content):
        # version.txt içinden yenilik notlarını çıkar
        start_index = version_txt_content.find('*')
        if start_index != -1:
            end_index = version_txt_content.find('\n', start_index)
            if end_index != -1:
                release_notes = version_txt_content[start_index:end_index].strip('* \t\n')
                return release_notes
        return "Yenilik notları bulunamadı."

class PowerPlanChanger(QWidget):
    def __init__(self):
        super().__init__()
        self.init_ui()

    def init_ui(self):
        self.setWindowTitle("Güç Planı Değiştirici")
        self.setGeometry(100, 100, 250, 120)

        main_layout = QVBoxLayout()

        self.filter_edit = QLineEdit()
        self.filter_edit.setPlaceholderText("Filtreleme metni girin")
        self.filter_edit.hide()
        self.filter_edit.setReadOnly(True)
        self.filter_edit.setText("guid")
        self.filter_edit.textChanged.connect(self.refresh_power_plans)

        main_layout.addWidget(self.filter_edit)

        self.plan_layout = QGridLayout()

        main_layout.addLayout(self.plan_layout)

        self.setLayout(main_layout)

        self.selected_button = None  # Seçili buton için referans

        # Güncelleme işlevi ekleniyor
        self.version_label = QLabel("Sürüm: 2.1", parent=self)
        self.version_label.hide()  # Sürüm etiketini gizle

        self.update_manager = UpdateManager(self.version_label)
        self.refresh_power_plans()

    def refresh_power_plans(self):
        try:
            self.clear_plan_layout()

            output = subprocess.check_output(['powercfg', '/l'], shell=True, stderr=subprocess.STDOUT)
            plans = output.decode('utf-8', errors='ignore').split('\r\n')
            plans = [plan.strip() for plan in plans if plan.strip()]

            filter_text = self.filter_edit.text().strip().lower()

            row = 0
            col = 0
            marked_plan = None
            for plan in plans:
                if self.is_guid(filter_text) and filter_text in plan.lower():
                    plan_name = re.search(r'\((.*?)\)', plan).group(1)  # Parantez içindeki kısmı alıyoruz
                    btn = QPushButton(plan_name)
                    btn.clicked.connect(lambda checked, p=plan: self.on_button_clicked(btn, p))
                    self.plan_layout.addWidget(btn, row, col)
                    if '*' in plan:  # Eğer plan işaretlenmişse
                        marked_plan = btn
                    row += 1
                    if row > 5:  
                        row = 0
                        col += 1
                elif not self.is_guid(filter_text) and filter_text in plan.lower():
                    plan_name = re.search(r'\((.*?)\)', plan).group(1)  
                    btn = QPushButton(plan_name)
                    on_button_click = functools.partial(self.on_button_clicked, btn, plan)
                    btn.clicked.connect(on_button_click)
                    self.plan_layout.addWidget(btn, row, col)
                    if '*' in plan:  
                        marked_plan = btn
                    row += 1
                    if row > 5:  
                        row = 0
                        col += 1

            if marked_plan:  # İşaretli plan varsa
                self.change_button_color(marked_plan)

        except subprocess.CalledProcessError as e:
            print(f"Hata: {e.output}")

    def clear_plan_layout(self):
        for i in reversed(range(self.plan_layout.count())):
            widget = self.plan_layout.itemAt(i).widget()
            if widget is not None:
                widget.deleteLater()

    def is_guid(self, text):
        pattern = re.compile(r'^[0-9a-fA-F]{8}-(?:[0-9a-fA-F]{4}-){3}[0-9a-fA-F]{12}$')
        return bool(pattern.match(text))

    def apply_power_plan(self, plan):
        try:
            plan_guid = plan.split()[3]
            subprocess.run(['powercfg', '/setactive', plan_guid], capture_output=True)
            print(f"{plan} planı aktif edildi.")
        except Exception as e:
            print(f"Hata: {e}")

    def on_button_clicked(self, button, plan):
        self.apply_power_plan(plan)  # Güç planını uygula
    
        # Tüm butonların arka plan rengini varsayılan yap
        for row in range(self.plan_layout.rowCount()):
            for col in range(self.plan_layout.columnCount()):
                item = self.plan_layout.itemAtPosition(row, col)
                if item and item.widget():
                    item.widget().setStyleSheet("")

        # Tıklanan butonun arka plan rengini yeşil yap
        button.setStyleSheet("background-color: green; color: white;")

    def change_button_color(self, button):
        if self.selected_button and self.selected_button != button:
            self.selected_button.setStyleSheet("")
        self.selected_button = button
        self.selected_button.setStyleSheet("background-color: green; color: white;")

def main():
    app = QApplication(sys.argv)
    window = PowerPlanChanger()
    window.show()
    sys.exit(app.exec_())

if __name__ == "__main__":
    main()
