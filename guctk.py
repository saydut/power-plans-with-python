import os
import subprocess
import requests
import re
import tempfile
import zipfile
import sys
from tkinter import Tk, Label, Button, Entry, messagebox, Frame, W, E, DISABLED

class UpdateManager:
    def __init__(self, version_label):
        self.version_label = version_label
        self.parent = version_label.master if version_label else None
        self.prog_ver_check()

    def apply_update(self, temp_exe_path, current_exe_path):
        try:
            os.replace(temp_exe_path, current_exe_path)
            return True
        except Exception as e:
            print(f"Hata: {e}")
            return False

    def download_update(self, url, dest_path):
        try:
            response = requests.get(url, stream=True)
            with open(dest_path, 'wb') as file:
                for chunk in response.iter_content(chunk_size=1024):
                    if chunk:
                        file.write(chunk)
        except Exception as e:
            print(f"Hata: {e}")

    def extract_zip(self, zip_path, extract_path):
        try:
            with zipfile.ZipFile(zip_path, 'r') as zip_ref:
                zip_ref.extractall(extract_path)
            return extract_path
        except Exception as e:
            print(f"Hata: {e}")
            return None

    def get_single_exe_in_directory(self, directory):
        exe_files = [f for f in os.listdir(directory) if f.lower().endswith('.exe') and os.path.isfile(os.path.join(directory, f))]

        if len(exe_files) == 1:
            return os.path.join(directory, exe_files[0])
        else:
            return None

    def create_update_script(self, temp_exe_path, current_exe_path):
        bat_content = f'@echo off\n'
        bat_content += f'taskkill /IM "{os.path.basename(current_exe_path)}" /F\n'
        bat_content += f'ping 127.0.0.1 -n 2 > nul\n'
        bat_content += f'start "" "{temp_exe_path}"\n'
        bat_content += f'exit\n'
        bat_path = os.path.join(tempfile.gettempdir(), "update_script.bat")
        with open(bat_path, 'w') as bat_file:
            bat_file.write(bat_content)
        return bat_path

    def run_update_script(self, bat_path):
        try:
            subprocess.run(bat_path, shell=True)
            return True
        except Exception as e:
            print(f"Hata: {e}")
            return False

    def prog_ver_check(self):
        gitlab_url = "https://gitlab.com/saydut/power-plans-with-python/-/raw/main/version.txt?ref_type=heads"

        local_version = self.version_label.cget("text").split(":")[-1].strip()

        try:
            response = requests.get(gitlab_url)
            remote_version_info = response.text.strip().split('\n')
            remote_version = remote_version_info[0]
            release_notes_start = response.text.find('*')
            release_notes = response.text[release_notes_start:].split('link=')[0].strip()
            update_link_start = response.text.find('link=')
            update_link = response.text[update_link_start + 5:].strip()

            if remote_version > local_version:
                notes_response = requests.get(gitlab_url)
                release_notes = self.extract_release_notes(notes_response.text)

                result = messagebox.askquestion("Yeni Sürüm", f"Yeni bir sürüm mevcut!\nGüncelleme yapmak ister misiniz?\n\n{release_notes}")

                if result == "yes":
                    print("Güncelleme işlemleri burada başlayacak.")
                    temp_zip_path = os.path.join(tempfile.gettempdir(), "temp_update.zip")
                    temp_exe_path = os.path.join(tempfile.gettempdir(), "temp_update.exe")

                    self.download_update(update_link, temp_zip_path)
                    extracted_path = self.extract_zip(temp_zip_path, tempfile.gettempdir())
                    exe_path = self.get_single_exe_in_directory(extracted_path)

                    bat_path = self.create_update_script(exe_path, sys.executable)
                    self.run_update_script(bat_path)
                    
                    sys.exit()
            else:
                print("Güncelleme yok.")
        except requests.RequestException as e:
            print(f"Hata: {e}")

    def extract_release_notes(self, version_txt_content):
        start_index = version_txt_content.find('*')
        if start_index != -1:
            end_index = version_txt_content.find('\n', start_index)
            if end_index != -1:
                release_notes = version_txt_content[start_index:end_index].strip('* \t\n')
                return release_notes
        return "Yenilik notları bulunamadı."

class PowerPlanChanger(Frame):
    def __init__(self, master=None):
        super().__init__(master)
        self.master = master
        self.icon_label = None
        self.init_ui()

    def init_ui(self):
        self.master.geometry("150x90")
        self.master.title("Güç Planı Değiştirici")
        self.master.resizable(False, False) # pencere boyutunu kilitleriz.
        self.master.pack_propagate(0)

        self.filter_edit = Entry(self.master, state=DISABLED, show='*')
        self.filter_edit.insert(0, "guid")
        self.filter_edit.grid(row=0, column=0, columnspan=2, sticky=W+E)
        self.filter_edit.bind("<KeyRelease>", self.refresh_power_plans)

        self.plan_frame = Frame(self.master)
        self.plan_frame.grid(row=1, column=0, columnspan=2)

        program_dizini = os.path.dirname(os.path.abspath(__file__))
        icon_path = os.path.join(program_dizini, "oig.ico")

        self.master.iconbitmap(icon_path)

        self.selected_button = None

        self.version_label = Label(self.master, text="Sürüm: 2.1")
        self.version_label.grid(row=2, column=0, columnspan=2)
        self.version_label.grid_remove()

        self.update_manager = UpdateManager(self.version_label)
        self.refresh_power_plans()

    def refresh_power_plans(self, event=None):
        try:
            self.clear_plan_frame()

            output = subprocess.check_output(['powercfg', '/l'], shell=True, stderr=subprocess.STDOUT)
            plans = output.decode('utf-8', errors='ignore').split('\r\n')
            plans = [plan.strip() for plan in plans if plan.strip()]

            filter_text = "guid"

            row = 0
            col = 0
            marked_plan = None
            for plan in plans:
                if self.is_guid(filter_text) and filter_text in plan.lower():
                    plan_name = re.search(r'\((.*?)\)', plan).group(1)
                    btn = Button(self.plan_frame, text=plan_name, command=lambda p=plan: self.on_button_clicked(p), highlightthickness=0, width=18, height=2)
                    btn.grid(row=row, column=col, pady=3, padx=10)
                    btn.config(state=DISABLED)
                    if '*' in plan:
                        marked_plan = btn
                    row += 1
                    if row > 5:
                        row = 0
                        col += 1
                elif not self.is_guid(filter_text) and filter_text in plan.lower():
                    plan_name = re.search(r'\((.*?)\)', plan).group(1)
                    btn = Button(self.plan_frame, text=plan_name, command=lambda p=plan: self.on_button_clicked(p), highlightthickness=0, width=18, height=1)
                    btn.grid(row=row, column=col, pady=3, padx=10)
                    if '*' in plan:
                        marked_plan = btn
                    row += 1
                    if row > 5:
                        row = 0
                        col += 1

                buton_genislik = 20
                buton_yukseklik = 2
                buton_arasi_bosluk = 2  # Butonlar arasındaki boşluk

                self.master.geometry("{}x{}".format(150 + col * (buton_genislik + buton_arasi_bosluk), 90 + (row * (buton_yukseklik + buton_arasi_bosluk))))


            if marked_plan:
                self.change_button_color(marked_plan)

            self.selected_button = None

        except subprocess.CalledProcessError as e:
            print(f"Hata: {e.output}")

        self.filter_edit.grid_forget()

    def clear_plan_frame(self):
        for widget in self.plan_frame.winfo_children():
            widget.destroy()

    def is_guid(self, text):
        pattern = re.compile(r'^[0-9a-fA-F]{8}-(?:[0-9a-fA-F]{4}-){3}[0-9a-fA-F]{12}$')
        return bool(pattern.match(text))

    def apply_power_plan(self, plan):
        try:
            plan_guid = plan.split()[3]
            subprocess.run(['powercfg', '/setactive', plan_guid], capture_output=True)
            print(f"{plan} planı aktif edildi.")
        except Exception as e:
            print(f"Hata: {e}")

    def on_button_clicked(self, plan):
        self.apply_power_plan(plan)

        for widget in self.plan_frame.winfo_children():
            widget.configure(bg=self.plan_frame.cget("bg"), foreground="black")

        plan_button = None
        for child in self.plan_frame.winfo_children():
            if child.cget("text") == re.search(r'\((.*?)\)', plan).group(1):
                plan_button = child
                break

        if plan_button:
            plan_button.configure(bg="green", fg="white")

        self.selected_button = plan

    def change_button_color(self, button):
        if self.selected_button and self.selected_button != button:
            self.selected_button.configure(bg=self.plan_frame.cget("bg"), foreground="black")
        self.selected_button = button
        self.selected_button.configure(bg="green", fg="white")
def main():
    root = Tk()
    app = PowerPlanChanger(root)
    root.mainloop()

if __name__ == "__main__":
    main()
