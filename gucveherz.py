import sys
import subprocess
from PyQt5.QtWidgets import QApplication, QWidget, QVBoxLayout, QGridLayout, QPushButton, QLineEdit, QComboBox, QLabel
import re
import functools
from ctypes import wintypes
import win32api
import win32con
import pywintypes
import ctypes
from contextlib import suppress

CHANGE_DPI_SCALE = False

class SYSTEM_POWER_STATUS(ctypes.Structure):
    _fields_ = [
        ('ACLineStatus', ctypes.c_ubyte),
        ('BatteryFlag', ctypes.c_ubyte),
        ('BatteryLifePercent', ctypes.c_ubyte),
        ('SystemStatusFlag', ctypes.c_ubyte),
        ('BatteryLifeTime', ctypes.wintypes.DWORD),
        ('BatteryFullLifeTime', ctypes.wintypes.DWORD),
    ]

def get_all_refresh_rates():
    i = 0
    refresh_rates = set()
    with suppress(Exception):
        while True:
            ds = win32api.EnumDisplaySettings(None, i)
            refresh_rates.add(ds.DisplayFrequency)
            i += 1
    return sorted(list(refresh_rates))

class PowerPlanChanger(QWidget):
    def __init__(self):
        super().__init__()
        self.init_ui()

    def init_ui(self):
        self.setWindowTitle("Güç Planı Değiştirici v2.1")
        self.setGeometry(100, 100, 250, 120)

        main_layout = QVBoxLayout()

        self.filter_edit = QLineEdit()
        self.filter_edit.setPlaceholderText("Filtreleme metni girin")
        self.filter_edit.hide()
        self.filter_edit.setReadOnly(True)
        self.filter_edit.setText("guid")
        self.filter_edit.textChanged.connect(self.refresh_power_plans)

        main_layout.addWidget(self.filter_edit)

        self.plan_layout = QGridLayout()

        main_layout.addLayout(self.plan_layout)

        self.setLayout(main_layout)

        self.selected_button = None  # Seçili buton için referans

        self.refresh_power_plans()

    def refresh_power_plans(self):
        try:
            self.clear_plan_layout()

            output = subprocess.check_output(['powercfg', '/l'], shell=True, stderr=subprocess.STDOUT)
            plans = output.decode('utf-8', errors='ignore').split('\r\n')
            plans = [plan.strip() for plan in plans if plan.strip()]

            filter_text = self.filter_edit.text().strip().lower()

            row = 0
            col = 0
            marked_plan = None
            for plan in plans:
                if self.is_guid(filter_text) and filter_text in plan.lower():
                    plan_name = re.search(r'\((.*?)\)', plan).group(1)  # Parantez içindeki kısmı alıyoruz
                    btn = QPushButton(plan_name)
                    btn.clicked.connect(lambda checked, p=plan: self.on_button_clicked(btn, p))
                    self.plan_layout.addWidget(btn, row, col)
                    if '*' in plan:  # Eğer plan işaretlenmişse
                        marked_plan = btn
                    row += 1
                    if row > 5:  
                        row = 0
                        col += 1
                elif not self.is_guid(filter_text) and filter_text in plan.lower():
                    plan_name = re.search(r'\((.*?)\)', plan).group(1)  
                    btn = QPushButton(plan_name)
                    on_button_click = functools.partial(self.on_button_clicked, btn, plan)
                    btn.clicked.connect(on_button_click)
                    self.plan_layout.addWidget(btn, row, col)
                    if '*' in plan:  
                        marked_plan = btn
                    row += 1
                    if row > 5:  
                        row = 0
                        col += 1

            if marked_plan:  # İşaretli plan varsa
                self.change_button_color(marked_plan)

        except subprocess.CalledProcessError as e:
            print(f"Hata: {e.output}")

    def clear_plan_layout(self):
        for i in reversed(range(self.plan_layout.count())):
            widget = self.plan_layout.itemAt(i).widget()
            if widget is not None:
                widget.deleteLater()

    def is_guid(self, text):
        pattern = re.compile(r'^[0-9a-fA-F]{8}-(?:[0-9a-fA-F]{4}-){3}[0-9a-fA-F]{12}$')
        return bool(pattern.match(text))

    def apply_power_plan(self, plan):
        try:
            plan_guid = plan.split()[3]
            subprocess.run(['powercfg', '/setactive', plan_guid], capture_output=True)
            print(f"{plan} planı aktif edildi.")
        except Exception as e:
            print(f"Hata: {e}")

    def on_button_clicked(self, button, plan):
        self.apply_power_plan(plan)  # Güç planını uygula
    
        # Tüm butonların arka plan rengini varsayılan yap
        for row in range(self.plan_layout.rowCount()):
            for col in range(self.plan_layout.columnCount()):
                item = self.plan_layout.itemAtPosition(row, col)
                if item and item.widget():
                    item.widget().setStyleSheet("")

        # Tıklanan butonun arka plan rengini yeşil yap
        button.setStyleSheet("background-color: green; color: white;")



    def change_button_color(self, button):
        if self.selected_button and self.selected_button != button:
            self.selected_button.setStyleSheet("")
        self.selected_button = button
        self.selected_button.setStyleSheet("background-color: green; color: white;")


class RefreshRateSwitcherApp(QWidget):
    def __init__(self):
        super().__init__()

        self.init_ui()

    def init_ui(self):
        self.setWindowTitle('Refresh Rate Switcher')
        self.setGeometry(100, 100, 400, 150)

        layout = QVBoxLayout()

        label = QLabel('Select a refresh rate:')
        layout.addWidget(label)

        refresh_rates = get_all_refresh_rates()

        self.refresh_rate_combobox = QComboBox()
        self.refresh_rate_combobox.addItems(map(str, refresh_rates))
        layout.addWidget(self.refresh_rate_combobox)

        apply_button = QPushButton('Apply')
        apply_button.clicked.connect(self.apply_refresh_rate)
        layout.addWidget(apply_button)

        self.setLayout(layout)

    def apply_refresh_rate(self):
        selected_refresh_rate = int(self.refresh_rate_combobox.currentText())
        set_refresh_rate(selected_refresh_rate)

def set_refresh_rate(refresh_rate: int):
    if refresh_rate:
        devmode = pywintypes.DEVMODEType()
        devmode.DisplayFrequency = refresh_rate
        devmode.Fields = win32con.DM_DISPLAYFREQUENCY

        win32api.ChangeDisplaySettings(devmode, 0)

def main():
    app = QApplication(sys.argv)
    
    power_plan_changer = PowerPlanChanger()
    refresh_rate_switcher = RefreshRateSwitcherApp()
    
    # Pencereleri yatay bir şekilde sıralamak için QHBoxLayout kullanabilirsiniz.
    main_layout = QVBoxLayout()
    main_layout.addWidget(power_plan_changer)
    main_layout.addWidget(refresh_rate_switcher)
    
    main_widget = QWidget()
    main_widget.setLayout(main_layout)
    
    main_widget.setWindowTitle("Güç ve Ekran Yöneticisi")
    
    main_widget.show()
    
    sys.exit(app.exec_())

if __name__ == "__main__":
    main()
